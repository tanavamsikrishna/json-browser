# A package to easily navigate a JSON Document

## Installation
### Using `straight` package manager
```elisp
(straight-use-package
 '(json-browser :type git :host bitbucket :repo "tanavamsikrishna/json-browser"))
(global-set-key (kbd "s-j") 'hydra-json-browser/body)
```
### Manually
Just download the `json-browser.el` file and load it
