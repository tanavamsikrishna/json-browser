;;; package --- json-browser

;; A utility to ease reading a JSON Document
;; Copyright (C) 2020 Vamsi Talupula

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; To jump around a json file easily

;;; Code:
(require 'json)
(require 'subr-x)
(require 'hydra nil t)
(require 'json-mode nil t)
(require 's)

(defconst json-browser/json-browser-buffer-name-format "*json-browser-[$0]*")
(defconst json-browser/json-browser-buffer-name-pattern "\*json-browser-\[.*\]\*")

(defun json-browser/new-json-browser-buffer ()
  "To open the buffer to deal with the JSON document."
  (interactive)
  (let ((json-browser-buf (s-format json-browser/json-browser-buffer-name-format 'elt (list (format-time-string "%F %I:%M:%S.%6N %p")))))
    (switch-to-buffer json-browser-buf)
    (with-current-buffer  json-browser-buf
      (if (featurep 'json-mode)
          (json-mode)))))

(defun json-browser/parse-json-browser-json-data ()
  "To parse the string in the current json-browser buffer."
  (interactive)
  (if (s-matches? json-browser/json-browser-buffer-name-pattern (buffer-name))
      (let* ((json-object-type 'hash-table)
             (json-array-type 'list)
             (json-key-type 'string)
             (json-browser-json-data nil))
        (progn
          (goto-char (point-min))
          (setq-local json-browser-json-data (json-read))
          (if (hash-table-p json-browser-json-data)
              (setq-local json-browser-json-data (gethash (completing-read "Key: " (hash-table-keys json-browser-json-data)) json-browser-json-data))
            (if (listp json-browser-json-data)
                (setq-local json-browser-json-data (nth (read-number (format "Enter [0-%s): " (safe-length json-browser-json-data))) json-browser-json-data)))
            )
          (erase-buffer)
          (insert (json-encode json-browser-json-data))
          (json-pretty-print-buffer)
          (goto-char (point-min))
          ))
    (message "Not in a json-browser buffer")))

(defun json-browser/kill-all-buffers ()
  "Kill all json-browser buffers."
  (interactive)
  (kill-matching-buffers json-browser/json-browser-buffer-name-pattern))

(defgroup json-browser nil
  "Group to customize JSON-Browser"
  :group 'tools)

(defcustom json-browser/create-hydra t
  "Whether to create the hydra for JSON-Browser.
Then you need to load json-browser package after Hydra package."
  :type 'boolean
  :group 'json-browser)

(when json-browser/create-hydra
  (defhydra json-browser/hydra (:hint nil)
    "
Json Path (_q_uit)
_O_pen Json Buffer, Paste & Format JSON document
_o_pen Json Buffer         _n_arrow down
_u_ndo                     _d_elete json-browser buffers
"
    ("o" json-browser/new-json-browser-buffer :color blue)
    ("O" (lambda () (interactive)
           (json-browser/new-json-browser-buffer)
           (erase-buffer)
           (yank)
           (json-pretty-print-buffer)) :color blue)
    ("n" json-browser/parse-json-browser-json-data :color pink)
    ("u" undo :color pink)
    ("d" json-browser/kill-all-buffers :color blue)
    ("q" nil)))

(provide 'json-browser)
;;; json-browser.el ends here
